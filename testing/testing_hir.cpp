/*
    -- MAGMA (version 2.0) --
       Univ. of Tennessee, Knoxville
       Univ. of California, Berkeley
       Univ. of Colorado, Denver
       @date

       @author Raffaele Solca
       @author Azzam Haidar
       @author Mark Gates

*/

// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// includes, project
#include "magma_v2.h"
#include "magma_lapack.h"
#include "testings.h"

#include "../control/magma_threadsetting.h"  // internal header

#define REAL

extern "C" void magma_ogita_diag( magma_int_t Nfound, double *dR, magma_int_t ldr, double *dS, magma_int_t lds, double *dW, magma_queue_t queue ); 
extern "C" void magma_ogita_r( magma_int_t Nfound, double *dR, magma_int_t ldr, double *dS, magma_int_t lds, double *dW, double delta, magma_queue_t queue ); 

static magma_int_t check_orthogonality(magma_int_t M, magma_int_t N, double *Q, magma_int_t LDQ, double eps);
static magma_int_t check_reduction(magma_uplo_t uplo, magma_int_t N, magma_int_t bw, double *A, double *D, magma_int_t LDA, double *Q, double eps );
static magma_int_t check_solution(magma_int_t N, double *E1, double *E2, double eps);

/* ////////////////////////////////////////////////////////////////////////////
   -- Testing dsygvdx
*/
int main( int argc, char** argv)
{
    TESTING_CHECK( magma_init() );
    magma_print_environment();

    real_Double_t gpu_time;

    double *h_A, *h_R, *h_work;
    float *h_sA, *h_sR, *h_swork;

    #ifdef COMPLEX
    double *rwork;
    magma_int_t lrwork;
    #endif

    double *w1, *w2;
    float *ws;
    magma_int_t *iwork;
    magma_int_t N, Nfound, n2, info, lda, lwork, liwork;
    magma_int_t info_ortho     = 0;
    magma_int_t info_solution  = 0;
    magma_int_t info_reduction = 0;
    magma_int_t ione = 1;
    int status = 0;

    magma_opts opts;
    opts.parse_opts( argc, argv );

    // pass ngpu = -1 to test multi-GPU code using 1 gpu
    magma_int_t abs_ngpu = abs( opts.ngpu );
    
    printf("%% jobz = %s, uplo = %s, ngpu %lld\n",
           lapack_vec_const(opts.jobz), lapack_uplo_const(opts.uplo),
           (long long) abs_ngpu);

    printf("%%   N     M  GPU Time (sec)   ||I-Q^H Q||/N   ||A-QDQ^H||/(||A||N)   |D-D_magma|/(|D| * N)\n");
    printf("%%=========================================================================================\n");
    magma_int_t threads = magma_get_parallel_numthreads();
    for( int itest = 0; itest < opts.ntest; ++itest ) {
        for( int iter = 0; iter < opts.niter; ++iter ) {
            N = opts.nsize[itest];
            lda = N;
            n2  = lda*N;

            magma_range_t range;
            magma_int_t il, iu;
            float vl, vu;
            opts.get_range( N, &range, &vl, &vu, &il, &iu );

            magma_ssyevdx_getworksize(N, threads, (opts.jobz == MagmaVec), 
                                     &lwork, 
                                     #ifdef COMPLEX
                                     &lrwork, 
                                     #endif
                                     &liwork);
            
            /* Allocate host memory for the matrix */
            TESTING_CHECK( magma_dmalloc_cpu( &h_A,   n2 ));
            TESTING_CHECK( magma_dmalloc_cpu( &w1,    N ));
            TESTING_CHECK( magma_dmalloc_cpu( &w2,    N ));
            TESTING_CHECK( magma_imalloc_cpu( &iwork, liwork ));
            
            TESTING_CHECK( magma_dmalloc_pinned( &h_R,    n2    ));
            TESTING_CHECK( magma_dmalloc_pinned( &h_work, lwork ));
            #ifdef COMPLEX
            TESTING_CHECK( magma_smalloc_pinned( &rwork, lrwork ));
            #endif

            TESTING_CHECK( magma_smalloc_cpu( &ws,    N ));
            TESTING_CHECK( magma_smalloc_cpu( &h_sA,    n2 ));
            TESTING_CHECK( magma_smalloc_pinned( &h_sR,    n2    ));
            TESTING_CHECK( magma_smalloc_pinned( &h_swork, lwork ));

            /* Initialize the matrix */
            magma_generate_matrix( opts, N, N, h_A, lda );

            if (opts.warmup) {
                // ==================================================================
                // Warmup using MAGMA
                // ==================================================================
                lapackf77_dlacpy( MagmaFullStr, &N, &N, h_A, &lda, h_R, &lda );
                if (opts.ngpu == 1) {
                    //printf("calling dsyevdx_2stage 1 GPU\n");
                    magma_dsyevdx_2stage( opts.jobz, range, opts.uplo, N, 
                                          h_R, lda, 
                                          vl, vu, il, iu, 
                                          &Nfound, w1, 
                                          h_work, lwork, 
                                          #ifdef COMPLEX
                                          rwork, lrwork, 
                                          #endif
                                          iwork, liwork, 
                                          &info );
                } else {
                    //printf("calling dsyevdx_2stage_m %lld GPU\n", (long long) opts.ngpu);
                    magma_dsyevdx_2stage_m( abs_ngpu, opts.jobz, range, opts.uplo, N, 
                                            h_R, lda, 
                                            vl, vu, il, iu, 
                                            &Nfound, w1, 
                                            h_work, lwork, 
                                            #ifdef COMPLEX
                                            rwork, lrwork, 
                                            #endif
                                            iwork, liwork, 
                                            &info );
                }
            }

            // ===================================================================
            // Performs operation using MAGMA
            // ===================================================================
            lapackf77_dlacpy( MagmaFullStr, &N, &N, h_A, &lda, h_R, &lda );
            lapackf77_dlag2s( &N, &N, h_A, &lda, h_sA, &lda, &info );
            lapackf77_slacpy( MagmaFullStr, &N, &N, h_sA, &lda, h_sR, &lda );
            gpu_time = magma_wtime();
            if (opts.ngpu == 1) {
                printf("calling dsyevdx_2stage 1 GPU\n");
                 /*magma_dsyevd( opts.jobz, opts.uplo, N,
                                      h_R, lda,
                                      w1,
                                      h_work, lwork,
                                      #ifdef COMPLEX
                                      rwork, lrwork,
                                      #endif
                                      iwork, liwork,
                                      &info );
                 Nfound = N;*/
                magma_ssyevdx_2stage( opts.jobz, range, opts.uplo, N,
                                      h_sR, lda,
                                      vl, vu, il, iu,
                                      &Nfound, ws,
                                      h_swork, lwork,
                                      #ifdef COMPLEX
                                      rwork, lrwork,
                                      #endif
                                      iwork, liwork,
                                      &info );
            } else {
                //printf("calling dsyevdx_2stage_m %lld GPU\n", (long long) opts.ngpu);
                magma_dsyevdx_2stage_m( abs_ngpu, opts.jobz, range, opts.uplo, N, 
                                        h_R, lda, 
                                        vl, vu, il, iu, 
                                        &Nfound, w1, 
                                        h_work, lwork, 
                                        #ifdef COMPLEX
                                        rwork, lrwork, 
                                        #endif
                                        iwork, liwork, 
                                        &info );
            }
            gpu_time = magma_wtime() - gpu_time;

            if (info != 0) {
                printf("magma_dsyevdx_2stage returned error %lld: %s.\n",
                       (long long) info, magma_strerror( info ));
            }
            
            lapackf77_slag2d( &N, &N, h_sR, &lda, h_R, &lda, &info );
            lapackf77_slag2d( &N, &ione, ws, &N, w1, &N, &info );

            //magma_dprint( N, N, h_A, lda );
            //magma_dprint( N, N, h_R, lda );

            printf("%5lld %5lld  %7.2f      ",
                   (long long) N, (long long) Nfound, gpu_time );
//R=eye(l)-X'*X;
//S=X'*A*X;
//lambda=diag(S)./(1-diag(R));
//D=diag(lambda);
//delta=2*(norm(S-D)+norm(A)*norm(R)); % 2-norm
//E=R/2;
//for i=1:l
//    for j=1:l
//        if abs(D(i,i)-D(j,j)) > delta
//            E(i,j)=(S(i,j)+D(j,j)*R(i,j)) / (D(j,j)-D(i,i));
//        end
//    end
//end
//X=X+X*E;

#if 1
            double *dA, *dV, *dR, *dS, *dtemp, *dW;
            int ldr = magma_roundup( Nfound, opts.align );
            int lds = ldr;
            int ldtemp = magma_roundup( N, opts.align );
            TESTING_CHECK( magma_dmalloc( &dA,    N*lda ));
            TESTING_CHECK( magma_dmalloc( &dV,    Nfound*lda ));
            TESTING_CHECK( magma_dmalloc( &dR,    ldr*Nfound ));
            TESTING_CHECK( magma_dmalloc( &dS,    lds*Nfound ));
            TESTING_CHECK( magma_dmalloc( &dtemp, ldtemp*Nfound ));
            TESTING_CHECK( magma_dmalloc( &dW, Nfound ));
            double d_zero = 0.0;
            double d_one = 1.0;
            double d_neg_one = -1.0;
            magma_dsetmatrix( N, N, h_A, lda, dA, lda, opts.queue );
            magma_dsetmatrix( N, Nfound, h_R, lda, dV, lda, opts.queue );
            magma_dsetmatrix( Nfound, 1, w1, Nfound, dW, Nfound, opts.queue );
            double normA = magmablas_dlange( MagmaFrobeniusNorm, N, N, dA, lda, dtemp, ldtemp*Nfound, opts.queue );
            //magma_dprint_gpu( 1, Nfound, dW, 1, opts.queue);
            double iter_time = magma_wtime();
            for(int it=0; it < 10; it++) {
                // R=eye(l)-X'*X;
                magmablas_dlaset( MagmaFull, Nfound, Nfound, d_zero, d_one, dR, ldr, opts.queue);
                magma_dgemm( MagmaTrans, MagmaNoTrans, Nfound, Nfound, N, d_neg_one, dV, lda, dV, lda, d_one, dR, ldr, opts.queue );
                // S=X'*A*X;
                //blasf77_dgemm( "N", "N", &N, &Nfound, &N, &d_one, h_A, &lda, h_R, &lda, &d_zero, temp, &ldtemp );
                magma_dsymm( MagmaLeft, opts.uplo, N, Nfound, d_one, dA, lda, dV, lda, d_zero, dtemp, ldtemp, opts.queue );
                magma_dgemm( MagmaTrans, MagmaNoTrans, Nfound, Nfound, N, d_one, dV, lda, dtemp, ldtemp, d_zero, dS, lds, opts.queue );
                //blasf77_dgemm( "T", "N", &Nfound, &N, &N, &d_one, h_R, &lda, h_A, &lda, &d_zero, temp, &Nfound );
                //blasf77_dgemm( "N", "N", &Nfound, &Nfound, &N, &d_one, temp, &Nfound, h_R, &lda, &d_zero, S, &lds );
                // lambda=diag(S)./(1-diag(R));
                /*for( int i=0; i<Nfound; i++) {
                    w1[i] = S[i*lds+i] / (1.0 - R[i*lds+i]);
                    S[i*lds+i] = S[i*lds+i] - w1[i];
                }*/
                magma_ogita_diag( Nfound, dR, ldr, dS, lds, dW, opts.queue );
                // delta=2*(norm(S-D)+norm(A)*norm(R));
                double delta = 2 * ( magmablas_dlange( MagmaFrobeniusNorm, Nfound, Nfound, dS, lds, dtemp, ldtemp*Nfound, opts.queue )
                             + normA * magmablas_dlange( MagmaFrobeniusNorm, Nfound, Nfound, dR, ldr, dtemp, ldtemp*Nfound, opts.queue ) );

                magma_ogita_r( Nfound, dR, ldr, dS, lds, dW, delta, opts.queue );
            
                /*for( int i=0; i<Nfound; i++ ) {
                    for ( int j=0; j<Nfound; j++ ) {
                        if ( abs(w1[i]-w1[j]) > delta ) {
                            R[i*ldr+j] = ( S[i*ldr+j] + w1[i]*R[i*ldr+j] ) / ( w1[i]-w1[j] );
                        } else {
                            R[i*ldr+j] = R[i*ldr+j] / 2;
                        }
                    }
                }*/

                // X=X+X*E;
                magma_dgemm( MagmaNoTrans, MagmaNoTrans, N, Nfound, Nfound, d_one, dV, lda, dR, ldr, d_one, dV, lda, opts.queue );

                //magma_dprint_gpu( 1, Nfound, dW, 1, opts.queue); 
                
            }

            iter_time = magma_wtime() - iter_time;
            printf("\n iter_time %f\n", iter_time/10.0);

#else
            double *R, *S, *temp;
            int ldr = magma_roundup( Nfound, opts.align );
            int lds = ldr;
            int ldtemp = magma_roundup( N, opts.align );
            TESTING_CHECK( magma_dmalloc_cpu( &R,    ldr*Nfound ));
            TESTING_CHECK( magma_dmalloc_cpu( &S,    lds*Nfound ));
            TESTING_CHECK( magma_dmalloc_cpu( &temp, ldtemp*Nfound ));
            double d_zero = 0.0;
            double d_one = 1.0;
            double d_neg_one = -1.0;
            double normA = lapackf77_dlange( "F", &N, &N, h_A, &lda, temp );
            double iter_time = magma_wtime();
            //printf("%f %f %f\n",d_zero, d_one, d_neg_one);
            //printf("%d\n", Nfound);
            /*printf("%d\n", lda);
            printf("%f %f %f\n", h_R[0], h_R[1], h_R[2]);
            printf("\n");
                for( int i=0; i<1600; i++ ) {
                    memcpy(temp, h_R+lda*i, sizeof(double)*N);
            printf("%f %f %f\n", h_R[0], h_R[1], h_R[2]);
            printf("%f %f %f\n", d_one, d_zero, d_neg_one);*/
            //        blasf77_dsymm("L", lapack_uplo_const(opts.uplo), &N, &ione, &d_neg_one, h_A, &lda, h_R+lda*i, &lda, /*w1+i*/&d_zero, h_R+lda*i, &lda );
            //printf("%f %f %f\n", h_R[0], h_R[1], h_R[2]);
            //printf("%f %f %f\n", d_one, d_zero, d_neg_one);
            //        w2[i] = lapackf77_dlange( "F", &N, &ione, h_R+lda*i, &lda, NULL ) / normA / lapackf77_dlange( "F", &N, &ione, temp, &lda, NULL );
            //        printf("%d\n", lda*i);
            //        printf( "%.16e %.16e %.16e %.16e\n", h_R[lda*i], lapackf77_dlange( "F", &N, &ione, h_R+lda*i, &lda, NULL ), normA, lapackf77_dlange( "F", &N, &ione, temp, &lda, NULL ) );
            //        fflush(stdout);
            //        memcpy(h_R+lda*i, temp, sizeof(double)*N);
            //    }
            //magma_dprint( 1, Nfound, w2, 1);
            for(int it=0; it < 10; it++) {
                // R=eye(l)-X'*X;
                lapackf77_dlaset( MagmaFullStr, &Nfound, &Nfound, &d_zero, &d_one, R, &ldr);
                blasf77_dgemm( "T", "N", &Nfound, &Nfound, &N, &d_neg_one, h_R, &lda, h_R, &lda, &d_one, R, &ldr );
                // S=X'*A*X;
                //blasf77_dgemm( "N", "N", &N, &Nfound, &N, &d_one, h_A, &lda, h_R, &lda, &d_zero, temp, &ldtemp );
                blasf77_dsymm("L", lapack_uplo_const(opts.uplo), &N, &Nfound, &d_one, h_A, &lda, h_R, &lda, &d_zero, temp, &ldtemp );
            printf("%f %f %f\n", temp[0], temp[1], temp[2]); fflush(stdout);
                blasf77_dgemm( "T", "N", &Nfound, &Nfound, &N, &d_one, h_R, &lda, temp, &ldtemp, &d_zero, S, &lds );
                //blasf77_dgemm( "T", "N", &Nfound, &N, &N, &d_one, h_R, &lda, h_A, &lda, &d_zero, temp, &Nfound );
                //blasf77_dgemm( "N", "N", &Nfound, &Nfound, &N, &d_one, temp, &Nfound, h_R, &lda, &d_zero, S, &lds );
                //printf("%f %f %f\n", S[0], S[ldr+1], S[2*ldr+2]);
                //printf("%e %e %e\n", R[0], R[ldr+1], R[2*ldr+2]);
                // lambda=diag(S)./(1-diag(R));
                for( int i=0; i<Nfound; i++) {
                    w1[i] = S[i*lds+i] / (1.0 - R[i*lds+i]);
                    S[i*lds+i] = S[i*lds+i] - w1[i];
                }
                //printf("%f %f %f\n", w1[0], w1[1], w1[2]);
                // delta=2*(norm(S-D)+norm(A)*norm(R));
                double delta = 2*( lapackf77_dlansy( "F", "U", &Nfound, S, &lds, temp ) + normA * lapackf77_dlange( "F", &Nfound, &Nfound, R, &ldr, temp ) );
            
                for( int i=0; i<Nfound; i++ ) {
                    for ( int j=0; j<Nfound; j++ ) {
                        if ( abs(w1[i]-w1[j]) > delta ) {
                            R[i*ldr+j] = ( S[i*ldr+j] + w1[i]*R[i*ldr+j] ) / ( w1[i]-w1[j] );
                        } else {
                            R[i*ldr+j] = R[i*ldr+j] / 2;
                        }
                    }
                }

                // X=X+X*E;
                blasf77_dgemm( "N", "N", &N, &Nfound, &Nfound, &d_one, h_R, &lda, R, &ldr, &d_one, h_R, &lda );

                for( int i=0; i<1600; i++ ) {
                    memcpy(temp, h_R+lda*i, sizeof(double)*N);
                    blasf77_dsymm("L", lapack_uplo_const(opts.uplo), &N, &ione, &d_neg_one, h_A, &lda, h_R+lda*i, &lda, w1+i, h_R+lda*i, &lda );
                    w2[i] = lapackf77_dlange( "F", &N, &ione, h_R+lda*i, &lda, NULL ) / normA / lapackf77_dlange( "F", &N, &ione, temp, &lda, NULL );
                    memcpy(h_R+lda*i, temp, sizeof(double)*N);
                }

                magma_dprint( 1, Nfound, w2, 1);
            }

            iter_time = magma_wtime() - iter_time;
            //printf("%f\n", iter_time);

#endif

            if ( opts.check ) {
                info_solution  = 0;
                info_ortho     = 0;
                info_reduction = 0;
                //double eps   = lapackf77_dlamch("E")*lapackf77_dlamch("B");
                double eps   = lapackf77_dlamch("E");
              
                /* Check the orthogonality, reduction and the eigen solutions */
                if (opts.jobz == MagmaVec) {
                    info_ortho = check_orthogonality(N, Nfound, h_R, lda, eps);
                    info_reduction = check_reduction(opts.uplo, N, 1, h_A, w1, lda, h_R, eps);
                } else {
                    printf("         ---                ---  ");
                }
                lapackf77_dsyevd("N", "L", &N, 
                                h_A, &lda, w2, 
                                h_work, &lwork, 
                                #ifdef COMPLEX
                                rwork, &lrwork, 
                                #endif
                                iwork, &liwork, 
                                &info);
                //magma_dprint( 1, Nfound, w2, 1);
                info_solution = check_solution(Nfound, w2, w1, eps);
                
                bool okay = (info_solution == 0) && (info_ortho == 0) && (info_reduction == 0);
                status += ! okay;
                printf("  %s", (okay ? "ok" : "failed"));
            }
            printf("\n");

            magma_free_cpu( h_A   );
            magma_free_cpu( w1    );
            magma_free_cpu( w2    );
            magma_free_cpu( iwork );
            
            magma_free_pinned( h_R    );
            magma_free_pinned( h_work );
            #ifdef COMPLEX
            magma_free_pinned( rwork  );
            #endif
            fflush( stdout );
        }
        if ( opts.niter > 1 ) {
            printf( "\n" );
        }
    }

    opts.cleanup();
    TESTING_CHECK( magma_finalize() );
    return status;
}



/*-------------------------------------------------------------------
 * Check the orthogonality of Q
 */
static magma_int_t check_orthogonality(magma_int_t M, magma_int_t N, double *Q, magma_int_t LDQ, double eps)
{
    double d_one     =  1.0;
    double d_neg_one = -1.0;
    double c_zero    = MAGMA_D_ZERO;
    double c_one     = MAGMA_D_ONE;
    double  normQ, result;
    magma_int_t     info_ortho;
    magma_int_t     minMN = min(M, N);
    double *work;
    TESTING_CHECK( magma_dmalloc_cpu( &work, minMN ));

    /* Build the idendity matrix */
    double *Id;
    TESTING_CHECK( magma_dmalloc_cpu( &Id, minMN*minMN ));
    lapackf77_dlaset("A", &minMN, &minMN, &c_zero, &c_one, Id, &minMN);

    /* Perform Id - Q^H Q */
    if (M >= N)
        blasf77_dsyrk("U", "C", &N, &M, &d_one, Q, &LDQ, &d_neg_one, Id, &N);
    else
        blasf77_dsyrk("U", "N", &M, &N, &d_one, Q, &LDQ, &d_neg_one, Id, &M);

    normQ = safe_lapackf77_dlansy("I", "U", &minMN, Id, &minMN, work);

    result = normQ / (minMN * eps);
    printf( "      %8.2e", normQ / minMN );

    // TODO: use opts.tolerance instead of hard coding 60
    if (std::isnan(result) || std::isinf(result) || (result > 60.0)) {
        info_ortho = 1;
    }
    else {
        info_ortho = 0;
    }
    magma_free_cpu( work );
    magma_free_cpu( Id   );
    
    return info_ortho;
}


/*------------------------------------------------------------
 *  Check the reduction 
 */
static magma_int_t check_reduction(magma_uplo_t uplo, magma_int_t N, magma_int_t bw, double *A, double *D, magma_int_t LDA, double *Q, double eps )
{
    double c_one     = MAGMA_D_ONE;
    double c_neg_one = MAGMA_D_NEG_ONE;
    double *TEMP, *Residual;
    double *work;
    double Anorm, Rnorm, result;
    magma_int_t info_reduction;
    magma_int_t i;
    magma_int_t ione=1;

    TESTING_CHECK( magma_dmalloc_cpu( &TEMP,     N*N ));
    TESTING_CHECK( magma_dmalloc_cpu( &Residual, N*N ));
    TESTING_CHECK( magma_dmalloc_cpu( &work,     N ));
    
    /* Compute TEMP =  Q * LAMBDA */
    lapackf77_dlacpy("A", &N, &N, Q, &LDA, TEMP, &N);        
    for (i = 0; i < N; i++) {
        blasf77_dscal(&N, &D[i], &(TEMP[i*N]), &ione);
    }
    /* Compute Residual = A - Q * LAMBDA * Q^H */
    /* A is symmetric but both upper and lower 
     * are assumed valable here for checking 
     * otherwise it need to be symetrized before 
     * checking.
     */ 
    lapackf77_dlacpy("A", &N, &N, A, &LDA, Residual, &N);        
    blasf77_dgemm("N", "C", &N, &N, &N, &c_neg_one, TEMP, &N, Q, &LDA, &c_one, Residual,     &N);

    // since A has been generated by larnv and we did not symmetrize, 
    // so only the uplo portion of A should be equal to Q*LAMBDA*Q^H 
    // for that Rnorm use dlansy instead of dlange
    Rnorm = safe_lapackf77_dlansy("1", lapack_uplo_const(uplo), &N, Residual, &N, work);
    Anorm = safe_lapackf77_dlansy("1", lapack_uplo_const(uplo), &N, A,        &LDA, work);

    result = Rnorm / ( Anorm * N * eps);
    printf("           %8.2e",  Rnorm / ( Anorm * N));

    // TODO: use opts.tolerance instead of hard coding 60
    if (std::isnan(result) || std::isinf(result) || (result > 60.0)) {
        info_reduction = 1;
    }
    else {
        info_reduction = 0;
    }

    magma_free_cpu( TEMP     );
    magma_free_cpu( Residual );
    magma_free_cpu( work     );

    return info_reduction;
}


/*------------------------------------------------------------
 *  Check the eigenvalues 
 */
static magma_int_t check_solution(magma_int_t N, double *E1, double *E2, double eps)
{
    magma_int_t   info_solution, i;
    double unfl   = lapackf77_dlamch("Safe minimum");
    double resid;
    double maxtmp;
    double maxdif = fabs( fabs(E1[0]) - fabs(E2[0]) );
    double maxeig = max( fabs(E1[0]), fabs(E2[0]) );
    for (i = 1; i < N; i++) {
        resid   = fabs(fabs(E1[i])-fabs(E2[i]));
        maxtmp  = max(fabs(E1[i]), fabs(E2[i]));

        /* Update */
        maxeig = max(maxtmp, maxeig);
        maxdif  = max(resid,  maxdif );
    }
    maxtmp = maxdif / max(unfl, eps*max(maxeig, maxdif));

    printf("              %8.2e", maxdif / (max(maxeig, maxdif)) );

    // TODO: use opts.tolerance instead of hard coding 100
    if (std::isnan(maxtmp) || std::isinf(maxtmp) || (maxtmp > 100)) {
        info_solution = 1;
    }
    else {
        info_solution = 0;
    }
    return info_solution;
}
